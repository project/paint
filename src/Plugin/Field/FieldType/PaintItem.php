<?php

namespace Drupal\paint\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'paint_paint' field type.
 *
 * @FieldType(
 *   id = "paint_paint",
 *   label = @Translation("Paint"),
 *   description = @Translation("Field to create a Canvas for paint."),
 *   category = @Translation("General"),
 *   default_widget = "paint_paint",
 *   default_formatter = "paint_paint_default",
 * )
 */
final class PaintItem extends FieldItemBase
{

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(
    FieldStorageDefinitionInterface $field_definition
  ): array {
    $properties['brush_size'] = DataDefinition::create('string')
      ->setLabel(t('Paint brush size'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(
    FieldStorageDefinitionInterface $field_definition
  ): array {
    $columns = [
      'brush_size' => [
        'type' => 'varchar',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(
    FieldDefinitionInterface $field_definition
  ): array {
    $random = new Random();

    $values['brush_size'] = $random->word(mt_rand(1, 10));

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool
  {
    return $this->brush_size === null;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array
  {
    $constraints = parent::getConstraints();

    return $constraints;
  }

}
