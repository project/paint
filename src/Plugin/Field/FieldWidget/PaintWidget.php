<?php

declare(strict_types=1);

namespace Drupal\paint\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'paint_paint' field widget.
 *
 * @FieldWidget(
 *   id = "paint_paint",
 *   label = @Translation("Paint"),
 *   field_types = {"paint_paint"},
 * )
 */
final class PaintWidget extends WidgetBase
{

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ): array {
    $element['brush_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Brush size'),
      '#default_value' => $items[$delta]->brush_size ?? '',
      '#size' => 20,
      '#min' => 1,
      '#max' => 10,
      '#description' => $this->t(
        'Select a Brush size between 1-10. Blank filed would not initiate the canvas.'
      )
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'container-inline';
    $element['#attributes']['class'][] = 'paint-paint-elements';
    $element['#attached']['library'][] = 'paint/paint_paint';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(
    array $element,
    ConstraintViolationInterface $error,
    array $form,
    FormStateInterface $form_state
  ): array|bool {
    $element = parent::errorElement($element, $error, $form, $form_state);
    if ($element === false) {
      return false;
    }
    $error_property = explode('.', $error->getPropertyPath())[1];
    return $element[$error_property];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(
    array $values,
    array $form,
    FormStateInterface $form_state
  ): array {
    foreach ($values as $delta => $value) {
      if ($value['brush_size'] === '') {
        $values[$delta]['brush_size'] = null;
      }
    }
    return $values;
  }

}
