<?php

declare(strict_types=1);

namespace Drupal\paint\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'paint_paint_default' formatter.
 *
 * @FieldFormatter(
 *   id = "paint_paint_default",
 *   label = @Translation("Paint"),
 *   field_types = {"paint_paint"},
 * )
 */
final class PaintDefaultFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array
  {
    $element = [];
    foreach ($items as $delta => $item) {
      if ($item->brush_size) {
        $element[$delta] = [
          '#type' => 'item',
          '#theme' => 'paint_image_formatter',
          '#canvasDetails' => [
            'id' => $items->getName() . '_' . $item->getName(),
            'brush_size' => $item->brush_size,
            'image_url' => ''
          ]
        ];
      }
    }

    $element['#attached']['library'] = ['paint/paint_paint'];

    return $element;
  }

}
