<?php

declare(strict_types=1);

namespace Drupal\paint\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileUrlGenerator;
use phpDocumentor\Reflection\DocBlock\Tags\Source;

/**
 * Plugin implementation of the 'Paint Image' formatter.
 *
 * @FieldFormatter(
 *   id = "paint_paint_image",
 *   label = @Translation("Paint Image"),
 *   field_types = {
 *     "entity_reference"
 *   },
 * )
 */
final class PaintImageFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array
  {
    $setting = ['brush_size' => '3'];
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition
  ) {
    if (array_key_exists(
      'image',
      $field_definition->getSetting('handler_settings')['target_bundles']
    )) {
      return $field_definition->getSetting('target_type') === 'media';
    } else {
      return $field_definition->getSetting('target_type') === '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $elements['brush_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Brush size'),
      '#default_value' => $this->getSetting('brush_size'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array
  {
    return [
      $this->t(
        'Brush Size: @brush_size',
        ['@brush_size' => $this->getSetting('brush_size')]
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array
  {
    $element = [];
    foreach ($items as $delta => $item) {
      if ($item->entity) {
        $fieldMachineName = $item->entity->getSource()->getConfiguration(
        )['source_field'];
        $imageUrl = $item->entity->$fieldMachineName->entity->uri->value;

        //TODO --- Get by glass
        $imageUrl = \Drupal::service(
          'file_url_generator'
        )->generateString($imageUrl);

        $element[$delta] = [
          '#type' => 'item',
          '#theme' => 'paint_image_formatter',
          '#canvasDetails' => [
            'id' => $items->getName() . '_' . $item->getName(),
            'brush_size' => $this->getSetting('brush_size') ?? 3,
            'imageDetails' => [
              'imageUrl' => $imageUrl ?? '',
              'imageWidth' => $item->entity->$fieldMachineName->width ?? '',
              'imageHeight' => $item->entity->$fieldMachineName->height ?? ''
            ]
          ]
        ];
      }
    }

    $element['#attached']['library'] = ['paint/paint_paint'];

    return $element;
  }

}
