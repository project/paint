/**
 * @file
 * Paint behaviors.
 */
(function (Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.paintPaint = {
    attach(context, settings) {

      if (createjs.Touch.isSupported()) {
        document.getElementsByClassName('html').add('touch');
      }
      var paintFieldList = document.getElementsByClassName('workspace');
      for (const [key, value] of Object.entries(paintFieldList)) {
        init(value.id);
      }
    }
  };

}(Drupal, drupalSettings));

function init(workspaceId) {
  var _canvas = document.getElementById(workspaceId);
  var _app = workspaceId.replace('workspace', 'app');
  var _appWidth = document.getElementById(_app).offsetWidth - 2; // -2 accounts for the border
  _canvas.width = _appWidth;
  _canvas.height = (_appWidth / 1.77); //16:9

  //stage canvas
  var _stage = new createjs.Stage(_canvas, {});
  _stage.enableDOMEvents(true);
  createjs.Touch.enable(_stage);

  var _shape = new createjs.Shape();
  var _isMouseDown = false;
  var _color = document.getElementById(_app).getElementsByClassName('paint__radio-brush')[5].value;
  var _isColorful = false;
  var _size = _canvas.getAttribute('data-brush-size');
  var _oldX, _oldY;

  //cursor
  var _cursor = new createjs.Shape();
  _cursor.graphics.beginFill(_color).drawCircle(0, 0, _size);
  _cursor.x = -5;
  _cursor.y = -5;

  var _image_path = _canvas.getAttribute('data-paint-image');
  var _image_width = _canvas.getAttribute('data-paint-image-width');
  var _image_height = _canvas.getAttribute('data-paint-image-height');

  var widthRatio = _canvas.width / _image_width;
  var heightRatio = _canvas.height / _image_height;
  _bgimage = new createjs.Bitmap(_image_path).set({scaleX: widthRatio, scaleY: heightRatio});
  _bgimage.x = 0;
  _bgimage.y = 0;
  if(_image_width < _image_height){
    var heightRatio = _canvas.height / _image_height;
    _bgimage = new createjs.Bitmap(_image_path).set({scaleX: heightRatio, scaleY: heightRatio});
    _canvas.width = _image_width * heightRatio;
  }

  if(_image_path !== ''){
    _stage.addChild(_bgimage, _shape, _cursor);
  } else {
    _stage.addChild(_shape, _cursor);
  }
  _stage.update();

  //events
  _stage.on("stagemousedown", function (evt) {
    _isMouseDown = true;
    //detect left mouse only
    if (evt.nativeEvent.button == 0) {
      //draw a dot whenever mouse down
      _shape.graphics.beginStroke(_color).setStrokeStyle(_size, 'round').lineTo(evt.stageX, evt.stageY);
      _stage.update();
    }
  });
  _stage.on('stagemouseup', function (evt) {
    _isMouseDown = false;
    //set random color
    if (_isColorful) {
      _color = createjs.Graphics.getRGB(Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255));

      // update new pen color based on the new generated color
      _cursor.graphics.clear();
      _cursor.graphics.beginFill(_color).drawCircle(0, 0, _size);
    }
    _stage.update();
  });
  _stage.on('stagemousemove', function (evt) {
    _cursor.x = evt.stageX;
    _cursor.y = evt.stageY;
    //start drawing
    if (_isMouseDown) {
      _shape.graphics.beginStroke(_color).setStrokeStyle(_size, 'dot').moveTo(_oldX, _oldY).lineTo(evt.stageX, evt.stageY);
    }
    _stage.update();
    _oldX = evt.stageX;
    _oldY = evt.stageY;
  });
  _stage.on('mouseenter', function () {
    _cursor.graphics.beginFill(_color).drawCircle(0, 0, _size);
    _stage.update();
  });
  _stage.on('mouseleave', function () {
    _cursor.graphics.clear();
    _stage.update();
  });

  // change Pen colors
  var rad_brush = document.getElementById(_app).getElementsByClassName('paint__radio-brush');
  for (const [key, value] of Object.entries(rad_brush)) {
    value.onchange = function () {
      _color = this.value;
      _isColorful = false;

      if (_color === 'colorful') {
        _isColorful = true;
        _color = createjs.Graphics.getRGB(Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255));
      }
      _cursor.graphics.beginFill(_color).drawCircle(0, 0, _size);
    }
  }

  var eraser_btn = document.getElementById(_app).getElementsByClassName('paint__btn-eraser')[0];
  eraser_btn.onclick = function () {
    _shape.graphics.clear();
    _stage.update();
  };

  // save image
  var save_btn = document.getElementById(_app).getElementsByClassName('paint__btn-save')[0];
  save_btn.onclick = function () {
    _stage.canvas.toBlob(function (blob) {
      var file = saveAs(blob, 'test.jpg');
    }, 'image/jpg');
  };
}